# Astronogeeks KSP Server

KSP Multiplayer server

## Getting Started

This project run with Docker

## Deployment

You need to build the Dockerfile 

```
docker build -t astronogeeks/server .
```

Then, run container

```
docker run -d --name astronogeeks-server -p 64738:64738 -p 80:80 -v /opt/applications/Astronogeeks/Universe:/DMPServer/Universe astronogeeks/server
```


## Authors

* **Jordan Roimarmier** - *Initial work* - [Chortan](https://gitlab.com/chortan)

See also the list of [contributors](https://gitlab.com/astronogeeks/server) who participated in this project.
