FROM mono:latest
RUN apt-get update && apt-get install -y unzip curl
RUN curl https://d-mp.org/builds/release/v0.3.2.1/DMPServer.zip --output DMPServer.zip
RUN unzip DMPServer.zip
WORKDIR ./DMPServer
CMD mono DMPServer.exe